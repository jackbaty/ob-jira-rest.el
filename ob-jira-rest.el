;;; ob-jira-rest.el --- org-babel functions for jira REST api

;; Original org-babel for restclient here:
;; https://github.com/alf/ob-restclient.el
;; Heavily modified by me.
;;
;; Copyright (C) 2015 Jason Stewart and Fusionary Media

;; Original Author: Alf Lervåg
;; Author: Jason Stewart
;; Keywords: literate programming, reproducible research
;; Homepage: http://orgmode.org
;; Version: 0.01

;;; License:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Commentary:
;; This is a very simple first iteration at integrating restclient.el
;; and org-mode to query JIRA for issues

;;; Requirements:
;; restclient.el

;;; Code:
(require 'ob)
(require 'ob-ref)
(require 'ob-comint)
(require 'ob-eval)
(require 'restclient)

(defvar org-babel-default-header-args:jira-rest
  '((:results . "raw"))
  "Default arguments for evaluating a jira-rest block.")

(defun jira-rest-template (user password endpoint max jql)
  "Turn USER PASSWORD ENDPOINT MAX and JQL into a restclient template."
  (let ((credentials (base64-encode-string (concat user ":" password)))
        (escaped-jql (replace-regexp-in-string "\"" "'" jql)))
    (format "POST %s
Content-Type: application/json
Authorization: Basic %s
{
  \"jql\": \"%s\",
  \"startAt\": 0,
  \"maxResults\": %s,
  \"fields\": [
    \"summary\"
  ]
}" endpoint credentials escaped-jql max)))

(defun build-jira-template (body params)
  "Initialize jira template wit BODY and PARAMS."

  (defvar jira-auth '((:user . nil) (:password . nil)))
  (if (file-exists-p "~/.jira-auth.el")
      (load-file "~/.jira-auth.el"))

  (let ((user (or (cdr (assoc :user params))
                  (cdr (assoc :user jira-auth))))
        (password (or (cdr (assoc :password params))
                      (cdr (assoc :password jira-auth))))
        (endpoint (or (cdr (assoc :rest-url params))
                      (cdr (assoc :rest-url jira-auth))))
        (max-results (or (cdr (assoc :max-results params))
                         100)))
    (jira-rest-template user password endpoint max-results body)))

(defun org-babel-execute:jira-rest (body params)
  "Execute a block of Restclient code with org-babel.
This function is called by `org-babel-execute-src-block'"
  (message "executing Jira-Rest source code block")
  (with-temp-buffer
    (let ((results-buffer (current-buffer))
          (restclient-same-buffer-response t)
          (restclient-same-buffer-response-name (buffer-name))
          (display-buffer-alist
           (cons
            '("\\*temp\\*" display-buffer-no-window (allow-no-window . t))
            display-buffer-alist)))

      (insert (buffer-name))
      (with-temp-buffer
        (insert (build-jira-template body params))
        (restclient-http-parse-current-and-do 'restclient-http-do nil t))

      (while restclient-within-call
        (sit-for 0.05))

      (goto-char (point-min))
      (when (search-forward (buffer-name) nil t)
        (error "Restclient encountered an error"))

      (let ((formatter (or (cdr (assoc :formatter params))
                           "list-format")))
        (org-babel-restclient-wrap-result formatter)))))

(defun parse-jira-response (json-string)
  "Jira gives a JSON-STRING and here it's turned into a JSON object.
The JSON object is then mapped to a list that contains the jira key and summary"
  (let ((json-object-type 'plist))
    (json-read-from-string (buffer-string))))

(defun jira-key-and-summary (jira-json)
  "Maps JIRA-JSON to proplist of :KEY summary."
  (let ((issues (plist-get jira-json :issues)))
    (mapcar
     (lambda (issue)
       (cons (plist-get issue :key)
             (plist-get (plist-get issue :fields) :summary)))
     issues)))

(defun org-table-format (jira-json)
  "Formats JIRA-JSON as an org table."
  (let ((result "|Issue Key|Summary\n|--\n"))
    (dolist (issue (jira-key-and-summary jira-json) result)
      (setq result (concat result "|" (car issue) "|" (cdr issue) "\n")))
    result))

(defun jira-list(jira-json tabs)
  "Formats JIRA-JSON as jira links.
Tabs will be inserted between the key and summary if the TABS options is present"
  (let ((result ""))
    (dolist (issue (jira-key-and-summary jira-json) result)
      (setq result (concat result "[[jira:" (car issue) "][" (car issue) "]]"
                           (if tabs "\t" " ") (cdr issue) "\n")))
    result))

(defun list-format (jira-json)
  "Formats JIRA-JSON as jira links."
  (jira-list jira-json nil))

(defun list-format-with-tabs (jira-json)
  "Formats JIRA-JSON as jira links.
Tabs will be inserted between the key and summary"
  (jira-list jira-json t))

(defun org-babel-restclient-wrap-result (formatter)
  "Process restclient results and format with FORMATTER with optional TABS.
Wrap the contents of the buffer in an `org-mode' src block."
  (let ((json-object-type 'plist))
    (let ((mode-name (substring (symbol-name major-mode) 0 -5))
          (parsed-json (parse-jira-response (buffer-string))))
      (insert (format "#+BEGIN_SRC %s\n" mode-name))
      (goto-char (point-max))
      (insert "#+END_SRC\n")
      (funcall (intern formatter) parsed-json))))

(provide 'ob-jira-rest)
;;; ob-jira-rest.el ends here
